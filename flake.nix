{
  description = "Weechat-matrix";

  inputs = {
    weechatMatrixSrc = {
      url = file:///git/github.com/poljar/weechat-matrix;
      ref = "master";
      type = "git";
      flake = false;
    };

    matrixNioSrc = {
      url = file:///git/github.com/poljar/matrix-nio;
      ref = "master";
      type = "git";
      flake = false;
    };

    unpaddedbase64Src = {
      url = file:///git/github.com/matrix-org/python-unpaddedbase64;
      ref = "master";
      type = "git";
      flake = false;
    };

    olmSrc = {
      url = file:///git/gitlab.matrix.org/matrix-org/olm;
      ref = "master";
      type = "git";
      flake = false;
    };

  };

  outputs = {
    self,
    weechatMatrixSrc, matrixNioSrc, unpaddedbase64Src,
    olmSrc
  }@fleiks: 

  {
    spici = "deriveicynLamdy";
    strok.spici = "iuniks";

    datom = { stdenv, git, python36, cmake }:
    let
      python = python36;

      inherit (python.pkgs) buildPythonPackage pyopenssl typing
      webcolors future atomicwrites cachetools attrs Logbook
      peewee pygments h11 h2 pycryptodome sphinx jsonschema
      aiohttp cffi aioresponses aiofiles dataclasses;

      olm = stdenv.mkDerivation {
        pname = "olm";
        version = olmSrc.shortRev;
        src = olmSrc;

        nativeBuildInputs = [ cmake ];

        doCheck = true;
      };

      python-olm = buildPythonPackage rec {
        pname = "python-olm";
        version = olmSrc.shortRev;
        src = olmSrc;

        sourceRoot = "source/python";
        buildInputs = [ olm ];

        preBuild = ''
          make include/olm/olm.h
        '';

        propagatedBuildInputs = [
          cffi
          future
          typing
        ];

        doCheck = false;
      };

      matrix-nio = buildPythonPackage rec {
        pname = "matrix-nio";
        version = matrixNioSrc.shortRev;
        src = matrixNioSrc;

        nativeBuildInputs = [ git ];

        propagatedBuildInputs = [
          attrs
          future
          aiohttp
          aiofiles
          h11
          h2
          Logbook
          jsonschema
          unpaddedbase64
          pycryptodome
          python-olm
          peewee
          cachetools
          atomicwrites
          dataclasses
        ];

        doCheck = false;
      };

      unpaddedbase64 = buildPythonPackage rec {
        pname = "unpaddedbase64";
        version = unpaddedbase64Src.shortRev;
        src = unpaddedbase64Src;
      };

      scriptPython = python.withPackages (ps: with ps; [
        aiohttp
        requests
        python_magic
      ]);

    in
    buildPythonPackage {
      pname = "weechat-matrix";
      version = weechatMatrixSrc.shortRev;
      src = weechatMatrixSrc;

      propagatedBuildInputs = [
        pyopenssl
        typing
        webcolors
        future
        atomicwrites
        attrs
        Logbook
        pygments
        cachetools
        matrix-nio
      ];

      passthru.scripts = [ "matrix.py" ];

      dontBuild = true;

      installPhase = ''
        mkdir -p $out/share $out/bin
        cp $src/main.py $out/share/matrix.py
        chmod +x $out/share/matrix.py

        cp $src/contrib/matrix_upload.py $out/bin/matrix_upload
        cp $src/contrib/matrix_decrypt.py $out/bin/matrix_decrypt
        cp $src/contrib/matrix_sso_helper.py $out/bin/matrix_sso_helper
        substituteInPlace $out/bin/matrix_upload \
        --replace '/usr/bin/env -S python3' '${scriptPython}/bin/python'
        substituteInPlace $out/bin/matrix_sso_helper \
        --replace '/usr/bin/env -S python3' '${scriptPython}/bin/python'
        substituteInPlace $out/bin/matrix_decrypt \
        --replace '/usr/bin/env python3' '${scriptPython}/bin/python'

        mkdir -p $out/${python.sitePackages}
        cp -r $src/matrix $out/${python.sitePackages}/matrix
      '';

      dontPatchShebangs = true;

      doCheck = false;
    };

  };
}
